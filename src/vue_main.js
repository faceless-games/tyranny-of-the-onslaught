"use strict";
exports.__esModule = true;
var ROT = require("rot-js");
window.onload = function () {
    console.log("Loaded!");
    var display = new ROT.Display();
    var container = document.getElementById("content");
    container.appendChild(display.getContainer());
    display.drawText(5, 2, "Hello, world!");
    var w = 39, h = 25;
    //let dm: ROT.Map;
    var dm = new ROT.Map.DividedMaze(w, h);
    for (var i = 0; i < 4; i++) {
        dm.create(display.DEBUG);
    }
};
