import * as ROT from 'rot-js'

import {Character} from "./character";
import {TileMap} from "./world";
import {GameMaster} from "./gamemaster";
import {MeleeCombatBehavior, PlayerBehavior, SitThereBehavior} from "./behavior";
import {Faction} from "./common";
import {IceSpikeAbility, LightningStrikeAbility} from "./ability";
import {Axe, EquippableItem, Rapier} from "./item";
import {EquipItemCommand} from "./commands";


export abstract class AbstractGameFactory { // What is this, Java?
    abstract newGame(): GameMaster;

    public gm:GameMaster;
    public player:Character;
    public map:TileMap;

    protected easySetup() {
        let player = new Character({
            glyph: '@',
            faction: Faction.HUMAN,
        });
        player.loc = {x: 1, y: 1}
        player.energy = 0;
        let map = new TileMap(40, 23);
        let mapGen = new ROT.Map.Arena(40, 23);
        mapGen.create(map.rotjsMapCallback());
        this.gm = new GameMaster(map, player);
        this.map = map;
        this.player = player;
        player.behavior = new PlayerBehavior(this.gm);

        return this;
    }

    public testMonster(overrides?: Partial<Character>): Character {
        let defaults = {
            name: "TestMonster McMonsterson",
            loc: {x: 2, y: 2},
            energy: -5,
            glyph: 't',
        }
        Object.assign(defaults, overrides);
        let npc = new Character(defaults);
        npc.behavior = new SitThereBehavior(this.gm);
        this.gm.addCharacter(npc);
        return npc;
    }

    public equip(item: EquippableItem, onWho: Character = null): EquippableItem {
        let target = onWho || this.player;
        let cmd = new EquipItemCommand(this.gm, target, item);
        cmd.doEquip();
        return item;
    }
}

export class NewGameFactory extends AbstractGameFactory {
    newGame(): GameMaster {
        this.easySetup();
        return this.gm;
    }
}

export class DebugGameFactory extends AbstractGameFactory {
    newGame(): GameMaster {
        this.easySetup();
        // let npc = this.testMonster({
        // });
        // npc.behavior = new MeleeCombatBehavior(this.gm);

        let blocker = this.map.tileAt({x: 3, y: 3});
        blocker.passable = false;
        blocker.glyph = "#";
        this.gm.recalculateFoV(this.player);

        this.player.gainAbility(new LightningStrikeAbility(this.gm, this.player));
        this.player.gainAbility(new IceSpikeAbility(this.gm, this.player));

        this.player.receiveItem(new Axe());
        this.player.receiveItem(new Rapier());

        return this.gm;
    }
}