import * as ROT from 'rot-js';
import {Color} from "rot-js/lib/color";
import {exportJsonExcept, Point, XY} from "./common";
import {Character} from "./character";

const NULL_GLYPH = 'X'

export class Tile extends Point {
    /* Fields that constructor creates:
    public glyph: string
    public location: XY
    public passable: boolean
    */

    static readonly NULL = new Tile(NULL_GLYPH, {x: -1, y: -1}, false);

    public character: Character;
    public lit: number;
    public viewed: boolean;

    //public contents: Array<Item>;

    static rotjsValueToTile(value: number, where: XY): Tile {
        switch(value) {
            case 0:
                return new Tile('.', where, true);
            case 1:
                return new Tile('#', where, false);
            default:
                return this.NULL;
        }
    }

    static glyphToTile(glyph: string, where: XY): Tile {
        let glyphIsPassable = false;
        switch(glyph) {
            case '.':
                glyphIsPassable = true;
                break;
            case ' ':
                // Blank space is not a valid glyph, but it makes typing in
                // the rooms manually a lot easier.
                glyphIsPassable = true;
                glyph = '.';
                break;
            default:
                glyphIsPassable = false;
        }
        return new Tile(glyph, where, glyphIsPassable);
    }

    public static fromJSON(template: Partial<Tile>) {
        let result = new Tile(
            template.glyph,
            {x: template.x, y: template.y},
            template.passable,
        )
        result.viewed = template.viewed;
        // TODO: Restore contents
        return result;
    }

    constructor(public glyph: string, location: XY, public passable = true) {
        super(location.x, location.y);
        this.viewed = false;
        this.glyph = glyph;
        // this.contents = [];
        // this.character = null;
    }

    public isNullTile(): boolean {
        return this.glyph === NULL_GLYPH;
    }

    displayedChar(includeCharacter = true): string {
        if(this.character && includeCharacter) {
            return this.character.glyph;
        }
        return this.glyph;
    }

    public fgcolor(lighting = 1.0): Color {
        let targetColor: Color = [255, 255, 255];
        return ROT.Color.interpolate([32, 32, 32], targetColor, lighting)
    }

    public toJSON() {
        return exportJsonExcept(this,
            'character',
        )
    }
}

export class TileMap {
    /* Fields that constructor creates:
    public width: number;
    public height: number;
    */

    public tiles: Array<Array<Tile>>;
    public open: Set<string>;

    public static fromJSON(template: Partial<TileMap>) {
        let {width, height, tiles} = template;
        let result = new TileMap(width, height);
        for(let x=0; x < result.width; x++) {
            for(let y=0; y < result.height; y++) {
                const xy = {x, y}
                let tilemplate = template.tiles[x][y];
                let tile = Tile.fromJSON(tilemplate);
                result.setTile(tile, xy);
            }
        }
        return result;
    }

    constructor(public width = 40, public height = 40) {
        let tiles = [];

        for(let x = 0; x < width; x++) {
            let col = []
            for(let y = 0; y < height; y++) {
                col.push(Tile.NULL);
            }
            tiles.push(col);
        }
        this.tiles = tiles;
        this.open = new Set<string>();
    }

    public addCharacter(character: Character, where?: XY): boolean {
        if(!where) where = character;
        let tile = this.tileAt(where);
        if(tile.isNullTile()) return false;
        if(tile.character) return false;

        tile.character = character;
        character.loc = where;
        return true;
    }

    public removeCharacter(character: Character) {
        let tile = this.tileAt(character);
        tile.character = null;
    }

    public rotjsMapCallback() {
        return (x: number, y: number, value: number) => {
            const where = {x, y};
            const newTile = Tile.rotjsValueToTile(value, where);
            this.setTile(newTile, where);
        }
    }

    public randomOpenTile(): Tile {
        if(this.open.size === 0) return null;
        // horribly inefficient, but we're choosing random tiles a lot less
        // often than setting tiles
        let openAry = new Array(...this.open);
        let chosen = ROT.RNG.getItem(openAry);
        let loc = JSON.parse(chosen);
        return this.tileAt(loc);
    }

    public setTile(val: Tile, where: XY = null) {
        let x, y;
        if(where === null) {
            ({x, y} = val);
        } else {
            ({x, y} = where);
            if(val !== Tile.NULL) {
                val.x = x;
                val.y = y;
            }
        }

        this.tiles[x][y] = val;
        let openkey = JSON.stringify(val.asXY());
        if(val.passable) {
            this.open.add(openkey);
        } else {
            this.open.delete(openkey);
        }
    }

    public tileAt(where: XY): Tile {
        if(!this.locationInBounds(where)) return Tile.NULL;
        return this.tiles[where.x][where.y];
    }

    public locationInBounds(loc: XY): boolean {
        const {x, y} = loc;
        if(x >= 0 && x < this.width) {
            if(y >= 0 && y < this.height) {
                return true;
            }
        }
        return false;
    }

    public debugPrint(): string {
        let rows = [];
        for(let y=0; y<this.height; y++) {
            let xtiles = [];
            for(let x=0; x<this.width; x++) {
                xtiles.push(this.tileAt({x, y}).displayedChar(false));
            }
            rows.push(xtiles.join(''));
        }
        return rows.join('\n');
    }
}
