import {GameMaster} from "../gamemaster";
import {jsonRoundTrip, TestGameFactory} from "./testutils";
import {Character} from "../character";
import {Tile, TileMap} from "../world";
import {MovementCommand, SitThereCommand} from "../commands";
import {PlayerBehavior, SitThereBehavior} from "../behavior";
import {DamageType, Energy, isEmptyObject} from "../common";

let tgf: TestGameFactory;
let gm: GameMaster;
let player: Character;
let pb: PlayerBehavior;
let map: TileMap;

beforeEach(() => {
    tgf = new TestGameFactory();
    tgf.newGame();

    ({gm, player, map, pb} = tgf.testGameData());
});

describe("Setup", () => {
    test("Basic setup", () => {
        expect(tgf.gm).toBeTruthy();
        expect(tgf.map).toBeTruthy();
        expect(tgf.player).toBeTruthy();

        // Character was set up
        expect(gm.characters).toHaveLength(1);
        expect(gm.characters[0]).toBe(player);
        expect(player.x).not.toEqual(-1);
        expect(player.y).not.toEqual(-1);

        // Placed on map correctly
        let tile = map.tileAt(player);
        expect(tile).toBeTruthy();
        expect(tile.isNullTile()).toBeFalsy();
        expect(tile.x).toStrictEqual(player.x);
        expect(tile.y).toStrictEqual(player.y);
    });
});

describe("Turn order", () => {
    test("Single", () => {
        player.energy = -5;

        expect(gm.activeCharacter).toBeFalsy();
        expect(gm.ready).toHaveLength(0);
        expect(player.energy).toBeLessThan(0);
        gm.supplyEnergy();
        expect(player.isReady()).toBeTruthy();
        expect(gm.ready).toHaveLength(1);
        expect(gm.ready[0]).toBe(player);

        let active = gm.determineActiveCharacter();
        expect(active).toBeTruthy();
        expect(active).toBe(player);
        expect(gm.activeCharacter).toBe(player);
        expect(gm.ready).toHaveLength(0);

        // Player has no command waiting, so nothing happens.
        expect(gm.takeTurn()).toBeFalsy();

        // Give them something to do
        pb.addCommand(new SitThereCommand(gm, player));
        expect(gm.takeTurn()).toBeTruthy();
        expect(gm.activeCharacter).toBeFalsy();
        expect(player.isReady()).toBeFalsy();

        // But they're up next because they're the only game in town
        gm.supplyEnergy();
        expect(player.isReady()).toBeTruthy();
    });

    test("Multiple", () => {
        player.energy = -5;
        let npc = new Character();
        gm.addCharacter(npc, {x: 3, y: 3});
        npc.energy = -7;

        gm.turn();
        // It should be the player's turn and they have no commends so it's still theirs
        expect(player.isReady()).toBeTruthy();
        expect(npc.isReady()).toBeFalsy();
        expect(npc.energy).toStrictEqual(-2);  // Should have applied 5 energy
        expect(gm.activeCharacter).toBe(player);

        // Give them something to do
        pb.addCommand(new SitThereCommand(gm, player));
        gm.turn();
        expect(gm.activeCharacter).toBeFalsy();
        expect(player.isReady()).toBeFalsy();
        // Turn() shouldn't have supplied additional energy because it was waiting on player
        expect(npc.energy).toStrictEqual(-2);

        let npcb = new PlayerBehavior(gm);
        npc.behavior = npcb;
        npcb.addCommand(new SitThereCommand(gm, npc));
        gm.turn();

        // Should now be nobody's turn
        expect(player.isReady()).toBeFalsy();
        expect(npc.isReady()).toBeFalsy();
        expect(npc.energy).toBeLessThan(-2);  // It did spend energy

        // But the player should be next
        gm.turn();
        expect(player.isReady()).toBeTruthy();
    });

    test("Dead monsters are pruned every turn", () => {
        player.energy = -5;
        let npc = new Character();
        gm.addCharacter(npc, {x: 3, y: 3});
        npc.energy = -7;

        gm.turn();
        // Give them something to do
        pb.addCommand(new SitThereCommand(gm, player));// Give them something to do
        // Pretend this somehow kills the npc
        npc.hp = 0;
        gm.turn();

        expect(gm.activeCharacter).toBeFalsy();
        expect(player.isReady()).toBeFalsy();
        expect(npc.alive).toBeFalsy();
        // GM should have taken it out of the character list
        expect(gm.characters).toHaveLength(1);
        expect(gm.characters[0]).toBe(player);
        // Also no longer on the map
        let tile = map.tileAt(npc);
        expect(tile.character).toBeFalsy();

    });
});

describe("Combat", () => {
    let npc: Character;
    beforeEach(() => {
        npc = tgf.testMonster();
    });

    test("Bump functionality", () => {
        pb.addCommand(new MovementCommand(gm, player, npc));

        gm.turn();
        expect(gm.activeCharacter).toBeFalsy();
        expect(player.isReady()).toBeFalsy();

        expect(player.energy).toStrictEqual(-Energy.STANDARD);
    });

    describe("Demonic Blood", () => {
        test("DB doeesn't increase for non-demon targets", () => {
            expect(player.demonicBlood).toStrictEqual(0);
            player.takeDamage(5, DamageType.PHYS, npc);
            expect(player.demonicBlood).toStrictEqual(0);
        });

        test("DB increases if demon is bloodied", () => {
            expect(player.demonicBlood).toStrictEqual(0);
            npc.takeDamage(3, DamageType.PHYS, player);
            expect(player.demonicBlood).toStrictEqual(3);
        });

        test("Overkill damage doesn't increase DB", () => {
            let origHp = npc.hp;
            expect(player.demonicBlood).toStrictEqual(0);
            npc.takeDamage(5000, DamageType.PHYS, player);
            expect(player.demonicBlood).toStrictEqual(origHp);
        });
    });
});

describe("FoV", () => {
    test("Calculated when added to GM", () => {
        let npc = new Character();
        expect(isEmptyObject(npc.visibleTiles)).toBeTruthy();
        expect(npc.canSee(npc)).toBeFalsy();

        gm.addCharacter(npc, {x: 3, y: 3});

        expect(isEmptyObject(npc.visibleTiles)).toBeFalsy();
        expect(npc.canSee(npc)).toBeTruthy();
        // Still can't see super far away though
        expect(npc.canSee({x: 30, y: 25})).toBeFalsy();
    });
})

describe("Serialization", () => {
    let npc: Character;

    beforeEach(() => {
    });

    test("Characters put back on their tiles", () => {
        tgf.moveCharacter(player, {x: 5, y: 2});
        npc = tgf.testMonster();

        let exported = jsonRoundTrip(gm);
        expect(exported['characters']).toHaveLength(1); // player omitted

        let newgame = GameMaster.fromJSON(exported);
        expect(newgame.characters).toHaveLength(2);
        let newTilePlayer = newgame.map.tileAt(player);
        expect(newTilePlayer.character).toBe(newgame.player);
        let newTileNpc = newgame.map.tileAt(npc);
        expect(newTileNpc.character).toBeTruthy();
    });

    test("Characters get GM injected into their behaviors", () => {
        let npc = tgf.testMonster( {
            loc: {x: 3, y: 3},
            energy: -2,
        });
        npc.behavior = new SitThereBehavior(gm);

        let exported = jsonRoundTrip(gm);
        let newgame = GameMaster.fromJSON(exported);
        let newNpc = newgame.characters.find(c => c !== newgame.player);
        expect(newNpc).toBeTruthy();
        expect(newNpc.behavior).toBeTruthy();
        expect(newNpc.behavior.char).toBe(newNpc);
        expect(newNpc.behavior.gm).toBe(newgame);
    });

    test("Player reinserted correctly", () => {
        tgf.moveCharacter(player, {x: 5, y: 2});
        let onTile = gm.map.tileAt(player.loc);
        let exported = jsonRoundTrip(gm);

        expect(exported['characters']).toHaveLength(0);

        let newgame = GameMaster.fromJSON(exported);
        expect(newgame.characters).toHaveLength(1);
        expect(newgame.characters[0]).toBe(newgame.player);

        expect(player.loc).toEqual({x: 5, y: 2});
        let newTile = gm.map.tileAt(player.loc)
        expect(newTile).toEqual(onTile);
        expect(newTile).toBeInstanceOf(Tile);
    });

    test("JSON exclusions", () =>{
        let exported = jsonRoundTrip(gm);
        expect(exported['activeCharacter']).toBeUndefined();
        expect(exported['ready']).toBeUndefined();

        expect(exported['characters']).toHaveLength(0);  // player not included
    });

});