import {AddableResult} from "../addable_result";

describe("AddableResult", () => {
    test("Additions do what I expect", () => {
        let result = new AddableResult(5);
        expect(result.value).toStrictEqual(5);

        result.add(2, "testing");
        expect(result.value).toStrictEqual(7);

        result.add(-10, "more testing");
        expect(result.value).toStrictEqual(-3);
    });

    describe("Minmax", () => {
        test("Lower bounds", () => {
            let result = new AddableResult(5);
            result.minimum = 10;
            expect(result.value).toStrictEqual(10);
        });

        test("Upper bounds", () => {
            let result = new AddableResult(5);
            result.maximum = 2;
            expect(result.value).toStrictEqual(2);
        });
    });

    test("Multipliers work how I expect", () => {
        let result = new AddableResult(10);
        expect(result.value).toStrictEqual(10);

        result.addMultiplier(1, "testing");
        expect(result.value).toStrictEqual(20);

        result.add(2, "testing flat number + multiplier");
        // 10+2 => 12 * (1+1) => 24
        expect(result.value).toStrictEqual(24);

        result.addMultiplier(-0.5, "testing negative multipliers");
        // 10+2 => 12 * (1+1-0.5) => 18
        expect(result.value).toStrictEqual(18);
    });

    test("Multipliers can't flip the number", () => {
        let result = new AddableResult(10);
        result.addMultiplier(-1, "testing");
        expect(result.value).toStrictEqual(0);

        result.addMultiplier(-1, "super negative");
        expect(result.value).toStrictEqual(0);

        // But those negative multipliers do have to be individually canceled out
        result.addMultiplier(1, "testing");
        expect(result.value).toStrictEqual(0);

        result.addMultiplier(1, "testing");
        expect(result.value).toStrictEqual(10);
    });

    test("Booleans work how I expect", () => {
        let result = new AddableResult();
        expect(result.passes).toBeTruthy();

        result.addBoolean(true, 'cuz');
        expect(result.passes).toBeTruthy();

        result.fail('epic');
        expect(result.passes).toBeFalsy();

        result.addBoolean(true, 'Redemption arc?');
        expect(result.passes).toBeFalsy();  // one fail = everything fails
    });
});
