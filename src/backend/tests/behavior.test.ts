import {TestGameFactory} from "./testutils";
import {GameMaster} from "../gamemaster";
import {Character} from "../character";
import {MeleeCombatBehavior, PlayerBehavior, SitThereBehavior} from "../behavior";
import {TileMap} from "../world";
import {RandomService} from "../random_service";
import {Ability} from "../ability";
import {SitThereCommand} from "../commands";

let tgf: TestGameFactory;
let gm: GameMaster;
let player: Character;
let npc: Character;
let pb: PlayerBehavior;
let map: TileMap;
let rng: RandomService;

let ability: Ability;

beforeEach(() => {
    tgf = new TestGameFactory();
    tgf.newGame();

    ({gm, player, map, pb} = tgf.testGameData());
    npc = tgf.testMonster({
        energy: -1,
        loc: {x: 5, y: 1},
    });
    pb = player.behavior as PlayerBehavior;
    rng = RandomService.instance;
});

function skipPlayer() {
    let cmd = new SitThereCommand(gm, player);
    pb.addCommand(cmd);
    gm.turn();
    gm.supplyEnergy();  // so NPC is ready
}

describe("SitThereBehavior", () => {
    beforeEach(() => {
        // I think it's the default
        expect(npc.behavior).toBeInstanceOf(SitThereBehavior);
    });

    test("It just... stands there", () => {
        let {x,y} = npc.loc;
        skipPlayer();
        expect(npc.isReady()).toBeTruthy();

        gm.turn();
        // NPC went
        expect(npc.isReady()).toBeFalsy();
        // They didn't move anywhere
        expect(x).toBe(npc.loc.x);
        expect(y).toBe(npc.loc.y);
    });
});

describe("MeleeCombatBehavior", () => {
    let behavior: MeleeCombatBehavior;
    beforeEach(() => {
        behavior = new MeleeCombatBehavior(gm);
        npc.behavior = behavior;
        skipPlayer();
    });

    test("Moving toward the player", () => {
        let {x,y} = npc.loc;
        expect(x).toStrictEqual(5);
        expect(y).toStrictEqual(1);
        let {x:px, y:py} = player.loc;
        expect(px).toStrictEqual(1);
        expect(py).toStrictEqual(1);

        expect(npc.isReady()).toBeTruthy();
        gm.turn();
        // npc went
        expect(npc.isReady()).toBeFalsy();

        expect(npc.loc.x).toStrictEqual(x - 1);
        expect(npc.loc.y).toStrictEqual(y);
    });

    test("NPC hits player in melee", () => {
        player.hp = 10000;

        // Put the npc right next to the player
        tgf.moveCharacter(npc, {x: 2, y: 1});
        expect(npc.loc.x).toStrictEqual(2);
        expect(npc.loc.y).toStrictEqual(1);

        expect(npc.isReady()).toBeTruthy();
        // Fight, fight, fight!
        rng.fix(3, 4);
        gm.turn();
        expect(npc.isReady()).toBeFalsy();

        // Didn't move because it was too busy fighting
        expect(npc.loc).toEqual({x: 2, y: 1});

        expect(player.hp).toEqual(9996);
    });

    test("Doesn't move toward player if it can't see them", () => {
        let {x,y} = npc.loc;
        let tile = map.tileAt({x: 2, y: 1});
        tile.passable = false;
        gm.recalculateFoV(npc);
        expect(npc.isReady()).toBeTruthy();

        gm.turn();

        // NPC went
        expect(npc.isReady()).toBeFalsy();
        // They didn't move anywhere
        expect(x).toBe(npc.loc.x);
        expect(y).toBe(npc.loc.y);
    });

    test("Moves toward player if player goes out of sight", () => {
        gm.turn();  // On their turn, they'll remember where the player is
        skipPlayer();
        let {x,y} = npc.loc;
        let tile = map.tileAt({x: 2, y: 1});
        tile.passable = false;
        gm.recalculateFoV(npc);
        expect(npc.isReady()).toBeTruthy();

        gm.turn();
        // NPC went
        expect(npc.isReady()).toBeFalsy();
        // They moved again
        expect(x).not.toBe(npc.loc.x);
    });
});