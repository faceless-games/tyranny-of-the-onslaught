import {Character, EquipmentSlots} from "./character";
import {SerializableCls, SerializableRegistry} from "./registry";
import {exportJsonExcept} from "./common";
import {Ability} from "./ability";
import {StatModifier, Tradeoff} from "./tradeoff";

export const ITEMTYPES = new SerializableRegistry<Item>();
const registerItemType = ITEMTYPES.registryDecorator();
type ItemTemplate = Partial<Item> & SerializableCls;

@registerItemType
export class Item {
    public desc: string
    public iid: string;
    public tradeoffs: Tradeoff[];

    // Need an easy way to hint to the inventory screen if this is something
    // that can be used or equipped
    public usable: boolean;
    public equippable: boolean;

    public readonly itemType: string;
    public owner: Character;

    public clsName: string;

    // Due to subclassing, just doing the usual `new Item(template)` may not be enough:
    static fromJSON(template: ItemTemplate): Item {
        let result = ITEMTYPES.newFromTemplate(template);
        result.realize();
        return result;
    }

    constructor(public name: string, public glyph: string) {
        this.usable = false;
        this.equippable = false;
        this.tradeoffs = [];
        this.setupDefaults();
        this.itemType = this.getItemType();
        this.clsName = this.constructor.name;
    }

    protected setupDefaults() {}

    protected realize() {
        this.tradeoffs = this.tradeoffs.map(t => Tradeoff.fromJSON(t));
    }

    public addTradeoff(tradeoff: Tradeoff) {
        this.tradeoffs.push(tradeoff);
    }

    public toJSON() {
        let result = exportJsonExcept(this,
            'owner',
        )
        return result;
    }

    public description(): string {
        return this.desc || '';
    }

    public getItemType(): string {
        return this.constructor.name;
    }
}

@registerItemType
export class EquippableItem extends Item {
    public slots: Array<keyof EquipmentSlots>;
    public intrinsic: StatModifier[];

    constructor(name = "ERROR", glyph = 'X') {
        super(name, glyph);
        this.equippable = true;
        this.slots = [];
        this.intrinsic = [];
    }

    public onEquippedBy(who: Character) {
        this.owner = who;
        for(let mod of this.intrinsic) {
            mod.applyModifier(who);
        }
        for(let trade of this.tradeoffs) {
            trade.apply(who);
        }
    }

    protected addIntrinsic(name: string, value: number) {
        this.intrinsic.push(new StatModifier(name, value));
    }

    protected realize() {
        super.realize();
        this.intrinsic = this.intrinsic.map(i => StatModifier.fromJSON(i));
    }
}

@registerItemType
export class Weapon extends EquippableItem {

    constructor(name='ERROR', glyph = '|') {
        super(name, glyph);
        this.slots = ['mainHand'];
    }
}

// SPECIFIC ITEMS FOLLOW

@registerItemType
export class Axe extends Weapon {
    constructor() {
        super('Axe');
        this.addIntrinsic('minWeaponDamage', 1);
        this.addIntrinsic('maxWeaponDamage', 6);
    }
}

@registerItemType
export class Rapier extends Weapon {
    constructor() {
        super('Rapier');
        this.addIntrinsic('minWeaponDamage', 1);
        this.addIntrinsic('maxWeaponDamage', 4);
        this.addIntrinsic('accuracy', 10);
    }
}