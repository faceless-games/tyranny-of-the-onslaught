export interface XY {
    x: number;
    y: number;
}

export interface XYWH extends XY {
    w: number;
    h: number;
}

export enum Energy {
    ROLL_INITIATIVE= 999,
    TURN = 13,
    STANDARD = 7,
    MOVE = 5,
    MINOR = 3,
    MIN = 1,
    ALL = -1,
}

export enum DamageType {
    PHYS,
    LIGHTNING,
    FIRE,
    COLD,
}

export enum Faction {
    DEMON,
    HUMAN,
    NOBODY,
    EVERYBODY,
}

export function removeFromArray<T>(item: T, array: Array<T>): T {
    for(let index=0; index < array.length; index++) {
        if(array[index] == item) {
            array.splice(index, 1);
            return item;
        }
    }
    return null;
}

export function exportJsonExcept(item: any, ...exclude: string[]): any {
    let result: any = {};
    let excluded = new Set(exclude);
    for(let key in item) {
        if(!excluded.has(key)) {
            result[key] = item[key];
        }
    }
    return result;
}

export function templateOverride(template: any, override?: any): any {
    let result = JSON.parse(JSON.stringify(template));
    if(override) {
        Object.assign(result, override);
    }
    return result;
}


// Courtesy: https://stackoverflow.com/a/21963136
export function uuidv4() {
    let u='',i=0;
    while(i++<36) {
        let c='xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'[i-1],r=Math.random()*16|0,v=c=='x'?r:(r&0x3|0x8);
        u+=(c=='-'||c=='4')?c:v.toString(16)
    }
    return u;
}

export function isEmptyObject(obj: any) {
    for (let x in obj) { return false; }
    return true;
}

export class Point implements XY {
    constructor(public x: number, public y: number) {

    }

    static fromLoc(loc: XY) {
        return new Point(loc.x, loc.y);
    }

    static add(origin: XY, offset: XY): Point {
        return new Point(
            origin.x + offset.x,
            origin.y + offset.y
        );
    }

    static subtract(dest: XY, src: XY): Point {
        return new Point(
            src.x - dest.x,
            src.y - dest.y,
        );
    }

    static distance(origin: XY, other: XY, topology = 8): number {
        let dx = Math.abs(origin.x - other.x);
        let dy = Math.abs(origin.y - other.y);
        if(topology === 8) {
            return Math.max(dx, dy);
        } else if(topology === 4) {
            return dx + dy;
        }
        // Everything else gets Euclidian distance
        return Math.sqrt(dx*dx + dy*dy);
    }

    static sameAs(origin: XY, other: XY): boolean {
        return origin.x === other.x && origin.y === other.y;
    }

    public sameAs(other: XY): boolean {
        return Point.sameAs(this, other);
    }

    public add(offset: XY): Point {
        return Point.add(this, offset);
    }

    public subtract(src: XY): Point {
        return Point.subtract(this, src);
    }

    public distance(other: XY, topology=8): number {
        return Point.distance(this, other, topology);
    }

    public fastNormalized(): Point {
        // Normalization how I'd expect to see in a roguelike
        // (i.e. no trig, just the direction that moves me closest)
        let dx = 0;
        let dy = 0;
        if(this.x < 0) dx = -1;
        if(this.x > 0) dx = 1;
        if(this.y < 0) dy = -1;
        if(this.y > 0) dy = 1;
        return new Point(dx, dy);
    }

    public asXY(): XY {
        return {
            x: this.x,
            y: this.y,
        }
    }
}

export class BoundingBox implements XYWH {

    public static centered(center: XYWH) {
        let halfWidth = Math.floor(center.w / 2);
        let x = center.x - halfWidth;
        let halfHeight = Math.floor(center.h / 2);
        let y = center.y - halfHeight;

        return new BoundingBox(x, y, center.w, center.h);
    }

    public static fromXYWH(xywh: XYWH) {
        return new BoundingBox(
            xywh.x,
            xywh.y,
            xywh.w,
            xywh.h,
        );
    }

    public constructor(
        public x: number,
        public y: number,
        public w: number,
        public h: number,
    ) {
    }

    public get center(): XY {
        return {
            x: this.x + Math.floor(this.w / 2),
            y: this.y + Math.floor(this.h / 2)
        };
    }

    public get bottom(): number {
        return this.y + this.h - 1;
    }

    public get right(): number {
        return this.x + this.w - 1;
    }

    public get upperLeft(): XY {
        return {
            x: this.x,
            y: this.y,
        };
    }

    public get upperRight(): XY {
        return {
            x: this.right,
            y: this.y,
        }
    }

    public get lowerLeft(): XY {
        return {
            x: this.x,
            y: this.bottom,
        }
    }

    public get lowerRight(): XY {
        return {
            x: this.right,
            y: this.bottom,
        }
    }

    public intersects(other: XYWH) {
        let bb = BoundingBox.fromXYWH(other);
        return !(
            this.right < bb.x  ||
            this.x > bb.right  ||
            this.bottom < bb.y ||
            this.y > bb.bottom
        );
    }

    public contains(point: XY) {
        return (
            point.x >= this.x &&
            point.x <= this.right &&
            point.y >= this.y &&
            point.y <= this.bottom
        )
    }

    public inset(amount: number): BoundingBox {
        return new BoundingBox(
            this.x + amount,
            this.y + amount,
            this.w - (amount * 2),
            this.h - (amount * 2),
        );
    }
}

export enum Targets {
    None,
    Self,
    Melee,
    Ranged,
    PBAoE,
    TAoE,
    Move,
}

interface IMessageable {
    msg(message: string): void;
}
interface IMessageHolder {
    messages: IMessageable;
}

export class MessageBuffer {
    public messages: string[][];
    protected msgRow: number;

    constructor() {
        this.messages = [];
        this.msgRow = -1;
    }

    public startMessage(msg: string): MessageBuffer {
        let row = [];
        row.push(msg);
        this.messages.push(row);
        this.msgRow += 1;

        return this;
    }

    public appendMessage(msg: string): MessageBuffer {
        let row = this.messages[this.msgRow];
        row.push(msg);
        return this;
    }

    public sendMessages(who: IMessageHolder) {
        for(let row of this.messages) {
            let text = row.join("");
            who.messages.msg(text);
        }
    }
}

export class MultiMessageBuffer extends MessageBuffer{
    protected buffers: MessageBuffer[];
    constructor(..._buffers: MessageBuffer[]) {
        super();
        this.buffers = _buffers;
    }

    startMessage(msg: string): MessageBuffer {
        for(let buffer of this.buffers) {
            buffer.startMessage(msg);
        }
        return this;
    }

    appendMessage(msg: string): MessageBuffer {
        for(let buffer of this.buffers) {
            buffer.appendMessage(msg);
        }
        return this;
    }

    sendMessages(who: IMessageHolder) {
        for(let buffer of this.buffers) {
            buffer.sendMessages(who);
        }
    }
}