import {defineConfig} from "vite";
import vue from '@vitejs/plugin-vue'

let revision = process.env['CI_COMMIT_SHA'];
if(!revision) {
    revision = require('child_process')
        .execSync('git rev-parse HEAD')
        .toString().trim()
}

let branch = process.env['CI_COMMIT_REF_NAME'];
if(!branch) {
    branch = require('child_process')
        .execSync('git symbolic-ref --short HEAD')
        .toString().trim()
}

const path = require("path");
export default defineConfig({
    plugins: [vue()],
    resolve: {
        alias: {
            "@": path.resolve(__dirname, "./src"),
        },
    },
    define: {
        '__GIT_COMMIT_SHA__': JSON.stringify(revision),
        '__GIT_BRANCH__': JSON.stringify(branch),
    },
});